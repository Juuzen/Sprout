package com.juuzen.sprout.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required
import org.bson.types.ObjectId
import java.util.*
import kotlin.math.max

open class Habit() : RealmObject() {

    @PrimaryKey
    var id : Int = -1

    var title : String = ""

    /*
    *
    *
     */
    var isPositive : Boolean = true

    var checkedActions : Int = 0

    /*
     * actionThreshold by default at 1
     * actionThreshold = null -> no limits
     */
    var actionThreshold : Int? = 1



    /* goals */
    var totalChecksGoal : Int? = null
    var streakLengthGoal : Int? = null
    var endDateGoal : Date? = null
    var totalActionsDoneGoal : Int? = null

    //TODO: impostare correttamente la data di creazione
    var creationDate : Date = Date()

    //TODO: gestire i rinvii
    //TODO: inserire variabile per la scelta di come gestire i rinvii (giornalmente, settimanalmente, mensilmente)
    //TODO: gestire i giorni attivi
    //TODO: gestire le notifiche
    //TODO: gestire l'albero
    //TODO: gestire i resultHistory
    //TODO: enum dei tipi
}