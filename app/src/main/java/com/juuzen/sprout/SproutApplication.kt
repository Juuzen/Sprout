package com.juuzen.sprout

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class SproutApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
    }
}