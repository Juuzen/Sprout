package com.juuzen.sprout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.juuzen.sprout.dao.HabitDao
import com.juuzen.sprout.model.Habit
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmConfiguration
import io.realm.kotlin.where

class MainActivity : AppCompatActivity() {
    private lateinit var realm: Realm
    private lateinit var realmChangeListener: RealmChangeListener<Realm>
    private lateinit var testButton: Button
    private lateinit var testCountText: TextView
    private lateinit var testDeleteButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        testButton = findViewById(R.id.testCountButton)
        testCountText = findViewById(R.id.testHabitNumber)
        testDeleteButton = findViewById(R.id.testDeleteButton)

        //FIXME: Dove va questo?
        val testConfig = RealmConfiguration.Builder()
            .name("test.realm")
            .schemaVersion(1)
            .allowQueriesOnUiThread(true)
            .allowWritesOnUiThread(true)
            .build()
        realm = Realm.getInstance(testConfig)
        realmChangeListener = RealmChangeListener {
            testCountText.text = realm.where<Habit>().count().toString()
        }
        realm.addChangeListener(realmChangeListener)

        testCountText.text = realm.where<Habit>().count().toString()

        testButton.setOnClickListener {
            val habitDao = HabitDao(realm)
            habitDao.testCreation()
        }

        testDeleteButton.setOnClickListener {
            val habitDao = HabitDao(realm)
            habitDao.deleteAllHabits()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        realm.removeChangeListener(realmChangeListener)
        realm.close()
    }
}