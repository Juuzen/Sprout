package com.juuzen.sprout.dao

import com.juuzen.sprout.model.Habit
import io.realm.Realm
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import java.util.*

class HabitDao(db: Realm) {
    private var realm: Realm = db

    private fun getMaxId() : Int {
        val maxId : Number? = realm.where<Habit>().max("id")
        return if (maxId == null) 0 else (maxId.toInt() + 1)
    }

    private fun formatDate() : String {
        return Calendar.getInstance().time.toString()
    }

    fun findById (habitId : Int) : Habit? {
        return realm.where<Habit>().equalTo("id", habitId).findFirst()
    }

    fun countAllHabits () : Long {
        return realm.where<Habit>().count()
    }

    fun testCreation() {
        realm.executeTransaction {
            val newHabit : Habit = realm.createObject(getMaxId())
            newHabit.title = "Test" + newHabit.id
            println("Habit creata il " + formatDate())
        }
    }

    fun deleteHabit(habitId: Int) {
        realm.executeTransaction {
            realm.where<Habit>().equalTo("id", habitId).findFirst()?.deleteFromRealm()
        }
    }

    fun deleteAllHabits() {
        realm.executeTransaction {
            val habits = realm.where<Habit>().findAll()
            habits.deleteAllFromRealm()
        }
    }
}